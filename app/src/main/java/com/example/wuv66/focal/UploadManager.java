package com.example.wuv66.focal;


import android.content.Context;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class UploadManager extends AsyncTask<String, String, String> {

    //String json_url;
    String JSON_STRING;
    Context context;

    public UploadManager(Context mcontext){
        context = mcontext;
    }

    @Override
    protected String doInBackground(String... params) {

        String url_link = params[0];    //"http://challenge.focal.systems/item";
        String id= params[1];
        String image = params[2];
        String name = params[3];
        String originalPrice = params[4];
        String priority = params[5];

        HttpURLConnection connection = null;
        BufferedReader reader = null;
        String line = "";
        String result = "";

        try{
            URL url = new URL(url_link);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.connect();

            //post the data
            OutputStream outputStream = connection.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            String post_data =
                    URLEncoder.encode("_id", "UTF-8") + "=" + URLEncoder.encode(id, "UTF-8") + "&" +
                            URLEncoder.encode("image", "UTF-8") + "=" + URLEncoder.encode(image, "UTF-8") + "&" +
                            URLEncoder.encode("name", "UTF-8") + "=" + URLEncoder.encode(name, "UTF-8") + "&" +
                            URLEncoder.encode("originalPrice", "UTF-8") + "=" + URLEncoder.encode(originalPrice, "UTF-8")+ "&" +
                            URLEncoder.encode("priority", "UTF-8") + "=" + URLEncoder.encode(priority, "UTF-8");
            bufferedWriter.write(post_data);
            bufferedWriter.flush();
            bufferedWriter.close();
            outputStream.close();


            //get the response code
            connection.setRequestMethod("GET");
            int responseCode = connection.getResponseCode();

            return Integer.toString(responseCode);

        } catch (MalformedURLException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void onPreExecute(){
    }

    @Override
    protected void onProgressUpdate(String... values){
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String result){
    }
}
