package com.example.wuv66.focal;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.concurrent.ExecutionException;

import io.reactivex.*;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {

    RecyclerView recycler;
    RecyclerAdapter adapter;
    PriorityQueue<RealmItem> queue;
    List failed = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        queue = new PriorityQueue<RealmItem>(200, new Comparator<RealmItem>() {
            public int compare(RealmItem a, RealmItem b) {
                if (a.priority < b.priority) return -1;
                if (a.priority > b.priority) return 1;
                return 0;
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View view) {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Help")
                        .setMessage("Swipe downwards on the gray area on the top of the screen. " +
                                "Data will then be downloaded from the server, parsed, and written to " +
                                "the Realm database. If there is any existing data, then it will be " +
                                "read from the database and loaded into the UI. While each item is being written " +
                                "to the database, a priority queue is running on a separate thread and uploading " +
                                "the items. If any items fail to be uploaded, an error dialog will show, and the items " +
                                "that were failed to be uploaded will display in the UI. Else, a success dialog " +
                                "will appear, notifying the user that all uploads were successful.")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });

        Realm realm = Realm.getDefaultInstance();

        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recycler = (RecyclerView) findViewById(R.id.recycler);

        recycler.setLayoutManager(layoutManager);


        adapter = new RecyclerAdapter();
        recycler.setAdapter(adapter);


        read();

        setSwipeDetector();
        rxUpload();

    }


    //for uploading
    public void rxUpload(){

        //Realm realm = Realm.getDefaultInstance();
        //results = realm.where(RealmItem.class).findAll();



        Observable<RealmItem> observable = Observable.create(new ObservableOnSubscribe<RealmItem>() {

            @Override
            public void subscribe(ObservableEmitter<RealmItem> e) throws Exception {
                //Use onNext to emit each item in the stream//

                System.out.println("Print queue size: " + queue.size());
                while(queue.size() > 0) {
                    RealmItem item = queue.poll();
                    e.onNext(item);
                }


                //Once the Observable has emitted all items in the sequence, call onComplete//
                e.onComplete();
            }}
        );

        observable.subscribe(new Observer<RealmItem>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull RealmItem item) {
                Log.d("STATE","subscribe: " + item.name);
                upload(item);
                //updateUI(item);
                //pollQueue();
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {
                if(adapter.getItemCount() != 0)
                    errorDialog();
                else
                    successDialog();
            }
        });

        observable.subscribeOn(AndroidSchedulers.mainThread())
             .observeOn(Schedulers.io())
             .map(this::upload);

    }


    //detect swipe down motion
    private void setSwipeDetector(){

        View homeView = (View) findViewById(R.id.swipe);

        homeView.setOnTouchListener(new OnSwipeListener(getApplicationContext()) {
            @Override
            public void onSwipeDown() {
                Log.d("STATE", "Swipe down detected!");
                download();
                rxUpload();
            }

        });
    }

    //download JSON data from url
    public void download(){
        DownloadManager manager = new DownloadManager(this);
        String url = "http://challenge.focal.systems/items";
        try {
            String result = manager.execute(url).get();
            if(result !=null) {
                Log.d("RESULT: ", result);
                parse(result);
            }
            else
                Log.d("RESULT: ", "error");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private RealmItem upload(RealmItem item){

        UploadManager manager = new UploadManager(this);
        try {
            String url = "http://challenge.focal.systems/item";
            String priority = String.valueOf(item.priority);
            String result = manager.execute(url, item.id, item.image, item.name, item.originalPrice, priority).get();
            if(result.equals("200")) {
                //upload succeeded
                Log.d("Upload success: ", result);
                remove(item);
            }
            else {
                //upload failed
                Log.d("Upload failed: ", result);
                failed.add(item.name);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return item;

    }

    public RealmItem remove(RealmItem item){

            adapter.remove(item);
            adapter.notifyDataSetChanged();
            System.out.println("Removing " + item.name);

            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            RealmItem result = realm.where(RealmItem.class).equalTo("id", item.id).findFirst();
            result.deleteFromRealm();
            realm.commitTransaction();
        return item;
    }

    //Convert JSON data into objects
    public void parse(String result) throws JSONException {
        JSONArray jsonarray = new JSONArray(result);
        RealmItem item = new RealmItem();
        for ( int i = 0; i < jsonarray.length() ; i++)
        {

            JSONObject object = jsonarray.getJSONObject(i);

            write(object);

        }
        Log.d("write size ", "" + jsonarray.length());
    }

    //Read from database to see if any objects already exist
    private void read(){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<RealmItem> results = realm.where(RealmItem.class).findAll();
        for(int i=0; i < results.size(); i++) {
            Log.d("READ ",results.get(i).name + ", priority: " + results.get(i).priority);
            updateUI(results.get(i));

        }
        Log.d("read size ", "" + results.size());
        //runQueue();
    }

    private RealmItem updateUI(RealmItem item){
        adapter.add(item);
        adapter.notifyDataSetChanged();

        queue.add(item);
        return item;
    }

    //start going through the queue to upload each item
    /*private void runQueue(){

        Log.d("queue size ", "" + queue.size());
        while(queue.size() != 0) {
            RealmItem item = queue.poll();
            System.out.println("Queue: " + item.name + ", priority: " + item.priority);
            //upload(item);
        }

        //if any items failed to upload, display error dialog
        errorDialog();
    }*/

    //Add object/data to database
    private void write(final JSONObject object) throws JSONException {
        final Realm realm = Realm.getDefaultInstance();
        //RealmItem item = new RealmItem();

        RealmItem exists = realm.where(RealmItem.class).equalTo("id", (String) object.get("_id")).findFirst();

        //if item is not in database, then add
        if(exists == null) {
            realm.beginTransaction();
            RealmItem item = realm.createObject(RealmItem.class, (String) object.get("_id")); // Create a new object
            item.name = (String) object.get("name");
            item.image = (String) object.get("image");
            item.originalPrice = (String) object.get("originalPrice");
            item.priority = (Integer) object.get("priority");

            Log.d("id: ", "" + item.id);
            Log.d("image: ", "" + item.image);
            Log.d("name: ", "" + item.name);
            Log.d("price: ", "" + item.originalPrice);
            Log.d("priority: ", "" + item.priority);
            realm.commitTransaction();

            //update UI
            updateUI(item);
        }
        //if already exists, don't add
        else
            Log.d("name: ", (String) object.get("name") + " already exists!");

    }

    private void errorDialog(){
        //if any items failed to upload, display error dialog
        if(failed.size() != 0){

            String list = "\n";
            for(int i = 0; i < failed.size();i++)
                list = list + "* " + failed.get(i)+ '\n';

            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("Upload error")
                    .setMessage("The following items have failed to upload: " + list)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
        failed.clear();
    }

    private void successDialog(){
        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Upload successful")
                .setMessage("Upload is successful. No items have failed to upload.")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
