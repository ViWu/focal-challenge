package com.example.wuv66.focal;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by wuv66 on 8/10/2017.
 */

public class RealmItem extends RealmObject {

    @PrimaryKey
    public String id;

    public String name, image, originalPrice;
    public int priority;



}
