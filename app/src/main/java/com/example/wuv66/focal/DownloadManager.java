package com.example.wuv66.focal;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class DownloadManager extends AsyncTask<String, String, String> {
    //String json_url;
    String JSON_STRING;
    Context context;

    public DownloadManager(Context mcontext){
        context = mcontext;
    }

    @Override
    protected String doInBackground(String... params) {

        String url_link = params[0];    //"http://challenge.focal.systems/items";

        HttpURLConnection connection = null;
        BufferedReader reader = null;
        String line = "";
        String result;

        try{
            URL url = new URL(url_link);
            connection = (HttpURLConnection) url.openConnection();
            //Log.d("RESPONSE CODE:", " " + connection.getResponseCode());
            connection.connect();

            InputStream stream = connection.getInputStream();

            reader = new BufferedReader(new InputStreamReader(stream));

            StringBuffer buffer = new StringBuffer();


            while((line = reader.readLine()) != null){
                //Log.d("Line read", "" + line);
                buffer.append(line);
            }

            result = buffer.toString();
            connection.disconnect();
            reader.close();
            return result;

        } catch (MalformedURLException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute(){
    }

    @Override
    protected void onProgressUpdate(String... values){
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String result){
    }

}