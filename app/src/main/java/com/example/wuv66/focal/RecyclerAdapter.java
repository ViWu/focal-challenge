package com.example.wuv66.focal;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerHolder> {

    //contains all the Realm objects to be displayed
    List list = new ArrayList();

    public class RecyclerHolder extends RecyclerView.ViewHolder {
        public TextView name, price, status;
        public String id;

        public RecyclerHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            price = (TextView) view.findViewById(R.id.price);

        }
    }


    public RecyclerAdapter() {

    }

    public void add(RealmItem item){
        list.add(item);
    }

    public RealmItem get(int index){
        return (RealmItem) list.get(index);
    }

    public void remove(RealmItem item){
        list.remove(item);
    }


    @Override
    public RecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_lv, parent, false);

        return new RecyclerHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerHolder holder, int position) {
        RealmItem item = (RealmItem) list.get(position);

        holder.name.setText(item.name);
        holder.price.setText(item.originalPrice);
        holder.id = item.id;
    }



    @Override
    public int getItemCount() {
        return list.size();
    }
}